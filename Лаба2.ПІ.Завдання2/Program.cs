﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаба2.ПІ.Завдання2
{
    class Program
    {
        static void RandomMatr (int[,] matr)
        {
            Random rnd = new Random();

            for (int i = 0; i < matr.GetLength(0); i++)
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    matr[i, j] = rnd.Next(0, 5);
                }        
        }

        static void PrintRandomMatr(int[,] matr)
        {
            Random rnd = new Random();

            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    Console.Write(matr[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void Mnozhennya(int[,] newmatr, int[,] firstmatr, int[,] secondmatr)
        {
            for (int r = 0; r < 3; r++)
            {
                for (int s = 0; s < 5; s++)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        newmatr[r, s] += firstmatr[r, i] * secondmatr[i, s];
                    }
                    Console.Write(" " + newmatr[r, s] + "");
                }
                Console.WriteLine();
            }
        }
       



        static void Main(string[] args)
        {
            int [,] firstmatr = new int [3,4];
            int [,] secondmatr = new int[4, 5];
            int[,] newmatr = new int[3, 5];



            RandomMatr(firstmatr);
            RandomMatr(secondmatr);

            PrintRandomMatr(firstmatr);
            Console.WriteLine( "\n");
            PrintRandomMatr(secondmatr);
            Mnozhennya(newmatr, firstmatr, secondmatr);


            Console.ReadLine();
        }
    }
}
