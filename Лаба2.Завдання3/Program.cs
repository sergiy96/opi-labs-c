﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаба2.Завдання3
{
    class Program
    {
        static Random rnd = new Random();

        static void SumRows(int[][] arr)
        {
            int[] arrSumRows = new int[arr.Length];
            int sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    sum += arr[i][j];
                }
                Console.Write(sum + " ");
                arrSumRows[i] = sum;
                sum = 0;
            }
            Console.WriteLine();

        }
        static void SumColumns(int[][] arr)
        {
            int max = 5;

            int[] arrSumCols = new int[5];
            int sum = 0;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (i < arr[j].Length) sum += arr[j][i];
                }
                Console.Write(sum + " ");
                arrSumCols[i] = sum;
                sum = 0;
            }
            Console.WriteLine();

        }


        static void Main(string[] args)
        {
            int[][] arr = new int[4][];
            arr[0] = new int[1];
            arr[1] = new int[2];
            arr[2] = new int[5];
            arr[3] = new int[3];
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    arr[i][j] = rnd.Next(0, 20);

                    Console.Write(arr[i][j] + " ");
                }
                Console.Write("\n");
            }

            Console.Write("сума рядк1в та стовпчик1в: \n");
            SumRows(arr);
            SumColumns(arr);
            Console.ReadLine();
        }
    }
}
