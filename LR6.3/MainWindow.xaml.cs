﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Converter;

namespace LR6._3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Temperature t;
        public MainWindow()
        {
            InitializeComponent();
            t = new Temperature();
        }

        private void textBoxC_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            t.Celsius = int.Parse(textBoxC.Text);
            textBoxF.Text = t.Fahrenheit.ToString("F2");
            textBoxK.Text = t.Kelvin.ToString("F2");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            t.Fahrenheit = int.Parse(textBoxF.Text);
            textBoxC.Text = t.Celsius.ToString("F2");
            textBoxK.Text = t.Kelvin.ToString("F2");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            t.Kelvin = int.Parse(textBoxK.Text);
            textBoxF.Text = t.Fahrenheit.ToString("F2");
            textBoxC.Text = t.Celsius.ToString("F2");
        }



    }
}
