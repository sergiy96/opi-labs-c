﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LR5
{
    public class ViewModel:ViewModelBase
    {
        private string _expression;
        private double _result;
        private ICommand _calc;
        public string Expression
        {
            get
            {
                return _expression;

            }
            set
            {
                _expression = value;
                RaisePropertyChanged(() => Expression);
                try
                {
                    Result = RPN.Calculate(_expression);
                }
                catch { }

            }
        }
        public double Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;
                RaisePropertyChanged(() => Result);
            }
        }
        public ICommand Calc
        {
            get
            {
                return _calc ?? (_calc = new RelayCommand(() =>
                {
                    Expression = RPN.Calculate(_expression).ToString();
                }));
            }
        }
    }
}
