﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LR5
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            fillValue();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Expression.Text += "1";
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Expression.Text += "2";
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Expression.Text += "3";
            fillValue();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Expression.Text += "4";
        }
        private void fillValue()
        {
            try
            {
                if (Expression.Text.StartsWith("Error"))
                    throw new Exception("Start with text");
                Value.Text = RPN.Calculate(Expression.Text).ToString() ;
            }
            catch
            {
                Value.Text = "Error";
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            Expression.Text = Value.Text;
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            Expression.Text += "5";
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            Expression.Text += "6";
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            Expression.Text += "7";
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            Expression.Text += "8";
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            Expression.Text += "9";
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            Expression.Text += "0";
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            Expression.Text += "+";
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            Expression.Text += "-";
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            Expression.Text += "*";
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            Expression.Text += "/";
        }

        private void Button_Click_15(object sender, RoutedEventArgs e)
        {
            Expression.Text += "^";
        }

        private void Button_Click_16(object sender, RoutedEventArgs e)
        {
            Expression.Text += "(";
        }

        private void Button_Click_17(object sender, RoutedEventArgs e)
        {
            Expression.Text += ")";
        }

        private void Button_Click_18(object sender, RoutedEventArgs e)
        {

            Expression.Text = "";
            Value.Text = "";
            
        }

        private void Button_Click_19(object sender, RoutedEventArgs e)
        {

            Expression.Text += ",";
        }
    }
}
