﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Converter;

namespace LR6._1
{
    class Program
    {
        static void Main(string[] args)
        {
            char type;
            double Temperature;

            Console.Write("Введіть шкалу температури: ");
            Char.TryParse(Console.ReadLine(), out type);
            Console.Write("Введіть температуру: ");
            Temperature = Double.Parse(Console.ReadLine());

            Temperature temp = new Temperature(type, Temperature);

            switch (type)
            {
                case 'c':
                    Console.WriteLine("K = {0}", temp.Kelvin);
                    Console.WriteLine("F = {0}", temp.Fahrenheit);
                    break;
                case 'k':
                    Console.WriteLine("C = {0}", temp.Celsius);
                    Console.WriteLine("F = {0}", temp.Fahrenheit);
                    break;
                case 'f':
                    Console.WriteLine("C = {0}", temp.Celsius);
                    Console.WriteLine("K = {0}", temp.Kelvin);
                    break;
                default: break;
            }
            

            Console.ReadKey();
        }
    }
}
