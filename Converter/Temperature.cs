﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter
{
    public class Temperature
    {
        private double temp;
        public static int Count;
        public Temperature()
        {
            temp = 0;
            Count++;
        }
        public Temperature(char type, double t)
        {
            switch (type)
            {
                case 'c':
                    {
                        Celsius = t;
                        break;
                    }
                case 'f':
                    {
                        Fahrenheit = t;
                        break;
                    }
                case 'k':
                    {
                        Kelvin = t;
                        break;
                    }
                default:
                    {
                        throw new Exception("Тип не підтримується\nСпробуйте: c, f або k.");
                    }
            }
        }
        static Temperature()
        {
            Count = 1;
        }
        public double Celsius
        {
            get
            {
                return temp - 273.15;
            }
            set
            {
                temp = value + 273.15;
            }
        }
        public double Kelvin
        {
            get
            {
                return temp;
            }
            set
            {
                temp = value;
            }
        }
        public double Fahrenheit
        {
            get
            {
                return 9 * (temp - 273.15F) / 5F + 32;//
            }
            set
            {
                temp = 5*(value - 32)/9 + 273.15;
            }
        }

        
    }
}
