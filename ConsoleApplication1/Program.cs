﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("a * x^4 + b * x^2 + c = 0\nВведiть параметри a,b,c");
            double a, b, c, x1,x2, d;
            a = double.Parse(Console.ReadLine());
            b = double.Parse(Console.ReadLine());
            c = double.Parse(Console.ReadLine());
            if(a!=0)
            {

                d = b * b - 4 * a * c;
                if(d>0)
                {
                    x1 = (b * (-1) + Math.Sqrt(d)) / (a * 2);
                    x2 = (b * (-1) - Math.Sqrt(d)) / (a * 2);
                    Console.WriteLine("x1 = " + x1.ToString());
                    Console.WriteLine("x2 = " + x2.ToString());
                    if (x1 >= 0)
                    {

                        Console.WriteLine("x = " + Math.Sqrt(x1).ToString());
                        Console.WriteLine("x = " + (Math.Sqrt(x1)*(-1)).ToString());
                    }
                    if (x2 >= 0)
                    {

                        Console.WriteLine("x = " + Math.Sqrt(x2).ToString());
                        Console.WriteLine("x = " + (Math.Sqrt(x2) * (-1)).ToString());
                    }
                   // if (x1 < 0 && x2 < 0) Console.WriteLine("Розв'язкiв немає");
                }
                else if (d < 0)
                {
                    Console.WriteLine("Розв'язкiв немає");

                } else
                {
                    x1 = (b * (-1) + Math.Sqrt(d)) / (a * 2);
                    if (x1 >= 0)
                    {

                        Console.WriteLine("x = " + Math.Sqrt(x1).ToString());
                        Console.WriteLine("x = " + (Math.Sqrt(x1) * (-1)).ToString());
                    }
                    else
                    {
                       Console.WriteLine("Розв'язкiв немає");
                    }
                    Console.WriteLine("x = " + x1.ToString());
                }
            }
            else
            {
                Console.WriteLine( "А не може дорiвнювати нулю");
            }
            Console.ReadKey();
        }
    }
}

