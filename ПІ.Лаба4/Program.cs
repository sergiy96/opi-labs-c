﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR4
{
    class Program
    {
        public static void printInfo()
        {
            Console.Clear();
            Console.WriteLine("1 - для виведення переліку імен");
            Console.WriteLine("2 - для додавання контакту");
            Console.WriteLine("3 - для редагування контакту");
            Console.WriteLine("4 - для видалення контакту");
            Console.WriteLine("5 - для відображення контакту");

        }
        
      
        public static void delete( Dictionary<string, List<int>> obj)
        {
            Console.WriteLine("Введіть ім'я");
            string key = Console.ReadLine();
            if (obj.ContainsKey(key))
            {
               obj.Remove(key);
               Console.WriteLine("Контакт {0} - видалено",key);
            }
            else
            {
                Console.WriteLine("Ім'я не знайдено");
            }
                Console.ReadKey(true);
        }
        public static void Show(Dictionary<string, List<int>> obj)
        {
            Console.WriteLine("Введіть ім'я");
            string key = Console.ReadLine();
            if (obj.ContainsKey(key))
            {
                Console.WriteLine(key+":");
                foreach(var i in obj[key])
                    Console.WriteLine("\t"+i.ToString());
            }
            else
            {
                Console.WriteLine("Ім'я не знайдено");
            }
            Console.ReadKey(true);
        }
        public static void Change(Dictionary<string, List<int>> obj)
        {
            string key;
            int n,newNumber;
            ConsoleKeyInfo KeyInfo;
            do
            {
                Console.Clear();
                Console.WriteLine("Введіть ім'я");
                key = Console.ReadLine();
                if (obj.ContainsKey(key))
                {
                    Console.WriteLine(key + ":");
                    for (int i = 0; i < obj[key].Count; i++)
                        Console.WriteLine(String.Format("\t{0}) {1}", i + 1, obj[key][i]));
                    Console.WriteLine("Для додавання нового номера введіть 0");
                    n = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введіть новий номер");
                    newNumber = int.Parse(Console.ReadLine());
                    obj[key].Add(newNumber);
                    }
                Console.WriteLine("Для продовження редагування натисніть Enter");
                KeyInfo = Console.ReadKey(true);
           } while (KeyInfo.Key == ConsoleKey.Enter);
        }
        public static void NumberAdd(Dictionary<string, List<int>> obj)
        {
            string key;
            Console.WriteLine("Введіть ім'я");
            key = Console.ReadLine();
            if (obj.ContainsKey(key))
            {
                Console.WriteLine("Список вже містить контакт - " + key);
                Console.ReadKey(true);
                return;
            }
            obj.Add(key, new List<int>());
            int tmp;
            Console.WriteLine("Введіть номери");
            Console.WriteLine("Для завершення - 0");
           
            do
            {
                tmp = int.Parse(Console.ReadLine());
                if (tmp == 0) return;
                obj[key].Add(tmp);

            } while (true);

        }
        public static void ContactsShow(Dictionary<string, List<int>> obj)
        {
            if (obj.Count == 0)
            {
                Console.WriteLine("Список імен порожній");
                return;
            }
            foreach (var i in obj)
            {
                Console.WriteLine(i.Key);
               // foreach (var j in i.Value)
                  //  Console.WriteLine(j.ToString());
            }
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.GetEncoding(1251);
            Dictionary<string, List<int>> contacts = new Dictionary<string, List<int>>();
            ConsoleKeyInfo key;
            do
            {
                printInfo();
                key =  Console.ReadKey(true);
                Console.Clear();
                switch (key.Key)
                {
                    case (ConsoleKey.NumPad1):
                        ContactsShow(contacts);
                        Console.ReadKey(true);
                        break;
                    case (ConsoleKey.NumPad2):
                        NumberAdd( contacts);
                        break;
                    case (ConsoleKey.NumPad3):
                        Change(contacts);
                        break;
                    case (ConsoleKey.NumPad4):
                        delete(contacts);
                        break;
                    case (ConsoleKey.NumPad5):
                        Show(contacts);
                        break;
                    defaut:break;
                }


            }while(key.Key != ConsoleKey.Escape);
        }
    }
}
