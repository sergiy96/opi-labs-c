﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Лаба3.Проект.Strings
{
    class Program
    {
        static void Dyzku(string text)
        {
            Console.WriteLine("---------Перев1рка дужок-------\n");
            string pattern1 = "\\(";// шаблон на відкриті дужки
            Regex d1 = new Regex(pattern1);
            int a = d1.Matches(text).Count;
            string pattern2 = "\\)";// шаблон на закриті дужки
            Regex d2 = new Regex(pattern2);
            int b = d2.Matches(text).Count;
            if (a == b)
                Console.WriteLine("зб1гається к1льк1сть дужок");
            else
                Console.WriteLine(" не зб1гається к1льк1сть дужок");
        }

        static void BiggestWord(string text)
        {
            Console.WriteLine("---------Найдовше слово-------\n");
            int Max = 0;// підрахунок символів
            string ResultText = "";
            string pattern=@"\w+" ;// "+" --якщо один і більше символів, що йдуть за w(символ)
            foreach (Match i in Regex.Matches(text,pattern))//обираєм слово,яке шукаєм із рядка
                if (i.Length > Max)
                {
                    Max = i.Length;
                    ResultText = i.Value;
                }
            Console.WriteLine(ResultText);
        }

        static void Kilkist(string text)
        {
            Console.WriteLine("---------К1льк1сть сл1в-------\n");
            string pattern = @"\w+";
            Console.WriteLine(Regex.Matches(text, pattern).Count); 
        }

        static void RizniSlova(string text) 
        {
            Console.WriteLine("---------К1льк1сть р1зних сл1в-------\n");
             string pattern = @"\w+"; 
             int kilkist=0;
             int flag = 0;
             MatchCollection arr = Regex.Matches(text, pattern);
             for (int i = 0; i < arr.Count; i++)
             {
                 for (int j =i+1; j < arr.Count; j++) //читаєм масив слів другий раз_i+1 = це як наступний елемент масиву ,бо під час першого проходження беремо нульовий елемент
                 {//порівння елементів масиву
                     if (arr[i].Value == arr[j].Value)
                         flag = 1;
                 }
                 if (flag == 0)
                     kilkist++;
                 else
                     flag = 0;

             }
             Console.WriteLine(kilkist);
        }

        static void Spaces(string text)
        {
            Console.WriteLine("---------Проб1ли-------\n");
            string pattern = "\\s+";
            Regex r = new Regex(pattern);
            string text2 = r.Replace(text, " ");
            Console.WriteLine(text2);
        }

        static void MasivSlivPodvLiter(string text)
        {
            Console.WriteLine("---------Слова з подвоєними л1терами-------\n");
            //string pattern = @"\w*(a{2,}|b{2,}|c{2,}|d{2,}|e{2,}|f{2,}|g{2,}|h{2,}|i{2,}|j{2,}|k{2,}|l{2,}|m{2,}|n{2,}|o{2,}|p{2,}|q{2,}|r{2,}|s{2,}|t{2,}|u{2,}|v{2,}|w{2,}|x{2,}|y{2,}|z{2,})\w*";
            string pattern = @"\w*(\w)\1\w*";
            MatchCollection arr = Regex.Matches(text, pattern);
            for (int i = 0; i < arr.Count; i++)
                Console.WriteLine(arr[i]);

        }

        static void Main(string[] args)
        {
            string text = "hello         world. hello  (Alex),  kiss me (    ()   ) Mary. hello Mary Dnipropetrovsk ";
            Console.WriteLine(text);
            Dyzku(text);
            BiggestWord(text);
            Kilkist(text);
            RizniSlova(text);
            Spaces(text);
            MasivSlivPodvLiter(text);
            Console.ReadLine();
        }
    }
} 
